#include <stdio.h>
#include <math.h>
#include <pico/stdlib.h>
#include "hardware/adc.h"

#define NUMADCBANKS 4
#define NUMADCCHANS 16
#define ADCPIN 28
#define BANKSEL0 27
#define BANKSEL1 26
#define CHANSEL0 22
#define CHANSEL1 21
#define CHANSEL2 20
#define CHANSEL3 19

extern uint16_t fun_adc_vals[NUMADCBANKS][NUMADCCHANS];

void joy8_setup_adc();
uint16_t joy8_adc_read(uint8_t current_bank, uint8_t current_chan);