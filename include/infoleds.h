#include <stdio.h>
#include <math.h>
#include <pico/stdlib.h>


void init_info_leds();
void info_leds_all_off();
void info_leds_all_off();
void info_leds_set(uint8_t pin_num, uint8_t state);
void info_leds_toggle(uint8_t pin_num);
void info_leds_toggle_all();
void info_leds_dance(uint32_t led_switch_delay,uint32_t dance_delay,uint32_t num_dances);