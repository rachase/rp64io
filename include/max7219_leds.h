#pragma once

#include <pico/stdlib.h>
#include "hardware/pio.h"

extern PIO leds_pio;
extern uint leds_sm;

void init_leds();
void leds_rando();
void leds_task(uint value);