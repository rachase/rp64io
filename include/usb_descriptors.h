#include "common/tusb_common.h"
#include "device/usbd.h"


#define NUMAXES 8

// working, 8 buttons 8 axes
#define JOYCON_REPORT_DESC_JOYPAD(...)                     \
    HID_USAGE_PAGE(HID_USAGE_PAGE_DESKTOP),                \
        HID_USAGE(HID_USAGE_DESKTOP_JOYSTICK),             \
        HID_COLLECTION(HID_COLLECTION_APPLICATION),        \
        __VA_ARGS__                                        \
        HID_USAGE_PAGE(HID_USAGE_PAGE_BUTTON),             \
        HID_USAGE_MIN(1),                                  \
        HID_USAGE_MAX(8),                                 \
        HID_LOGICAL_MIN(0),                                \
        HID_LOGICAL_MAX(1),                                \
        HID_REPORT_SIZE(1),                                \
        HID_REPORT_COUNT(8),                              \
        HID_INPUT(HID_DATA | HID_VARIABLE | HID_ABSOLUTE), \
        HID_USAGE_PAGE(HID_USAGE_PAGE_DESKTOP),            \
        HID_LOGICAL_MIN(0x00),                             \
        HID_LOGICAL_MAX_N(0x0FFF, 2),                      \
        HID_PHYSICAL_MIN(0x00),                            \
        HID_PHYSICAL_MAX_N(0x0FFF,2),                      \
        HID_REPORT_SIZE(16),                               \
        HID_REPORT_COUNT(8),                               \
        HID_USAGE(HID_USAGE_DESKTOP_X),                    \
        HID_USAGE(HID_USAGE_DESKTOP_Y),                    \
        HID_USAGE(HID_USAGE_DESKTOP_Z),                    \
        HID_USAGE(HID_USAGE_DESKTOP_RX),                   \
        HID_USAGE(HID_USAGE_DESKTOP_RY),                   \
        HID_USAGE(HID_USAGE_DESKTOP_RZ),                   \
        HID_USAGE(HID_USAGE_DESKTOP_SLIDER),               \
        HID_USAGE(HID_USAGE_DESKTOP_DIAL),                 \
        HID_INPUT(HID_DATA | HID_VARIABLE | HID_ABSOLUTE), \
        HID_COLLECTION_END

