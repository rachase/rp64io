#pragma once

#include <pico/stdlib.h>
#include "hardware/pio.h"

void init_switches();
void switches_task();
uint64_t switches_get();


