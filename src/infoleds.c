#include "infoleds.h"

// the gpio numbers of the rp pico 
#define NUM_INFO_LEDS 10
uint8_t info_leds[NUM_INFO_LEDS]={0,1,4,5,6,11,12,18,17,16};

void init_info_leds()
{
    for (int i =0 ; i < NUM_INFO_LEDS; i++)
    {
        // Initialize LED pin
        gpio_init(info_leds[i]);
        gpio_set_dir(info_leds[i], GPIO_OUT);
    }
}

void info_leds_all_off()
{
    for (int i =0 ; i < NUM_INFO_LEDS ; i++)
    {
        gpio_put(info_leds[i],0);
    }
}

void info_leds_all_on()
{
    for (int i =0 ; i < NUM_INFO_LEDS ; i++)
    {
        gpio_put(info_leds[i],1);
    }
}

void info_leds_toggle(uint8_t pin_num)
{
    uint32_t mask;
    if (pin_num < NUM_INFO_LEDS)
    {
        // get the pin number
        uint8_t val = info_leds[pin_num];
        mask = 1<<val; 
        gpio_xor_mask(mask);
    }   
}

void info_leds_set(uint8_t pin_num, uint8_t state)
{
    uint32_t mask;
    if (pin_num < NUM_INFO_LEDS)
    {
        // get the pin number
        uint8_t val = info_leds[pin_num];
        gpio_put(val,state);
    }   
}

void info_leds_toggle_all()
{
    for (int i =0 ; i < NUM_INFO_LEDS ; i++)
    {
        info_leds_toggle(i);
    }
}

// led_switch_delay is the delay between moving from 1 led to the next
// dance_delay is how long to wait after each dance.
// num_dances is the number of times to do dance. 
void info_leds_dance(uint32_t led_switch_delay,uint32_t dance_delay,uint32_t num_dances)
{
    // get the state of the info pins
    uint8_t info_leds_state[NUM_INFO_LEDS];

    for (int i =0 ; i < NUM_INFO_LEDS ; i++)
    {
        // get the current pin state, then clear it.
        info_leds_state[i] = gpio_get_out_level(info_leds[i]);
        gpio_put(info_leds[i],0);
    }

    // now dance!
    for (int j = 0 ; j < num_dances ; j++)
    {
        for (int i =0 ; i < NUM_INFO_LEDS ; i++)
        {
            gpio_put(info_leds[i],1);
            sleep_ms(led_switch_delay);
            gpio_put(info_leds[i],0);
        }
        for (int i = NUM_INFO_LEDS ; i >0  ; i--)
        {
            gpio_put(info_leds[i-1],1);
            sleep_ms(led_switch_delay);
            gpio_put(info_leds[i-1],0);
        }
        gpio_put(info_leds[0],1);
        sleep_ms(dance_delay);
    }

    //restore state
    for (int i =0 ; i < NUM_INFO_LEDS ; i++)
    {
        gpio_put(info_leds[i],info_leds_state[i]);
    }
}
