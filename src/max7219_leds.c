#include "max7219_leds.h"
#include "max7219.pio.h"

#include "hardware/regs/rosc.h"
#include "hardware/regs/addressmap.h"

#define CLOCK_PIN  15
#define MOSI_PIN 14
#define CS_PIN 13

PIO leds_pio;
uint leds_sm;


uint8_t rnd(void){
    int k;
    uint8_t random=0;
    volatile uint32_t *rnd_reg=(uint32_t *)(ROSC_BASE + ROSC_RANDOMBIT_OFFSET);
    
    for(k=0;k<8;k++){
    
    random = random << 1;
    random=random + (0x01 & (*rnd_reg));

    }
    return random;
}

void max7219_setleds(PIO pio, uint sm);
void max7219_pio_setup();
void max7219_init(PIO pio, uint sm);
void max7219_setleds(PIO pio, uint sm);


void leds_task(uint value)
{
    max7219_setleds(leds_pio, value);
}


void leds_rando()
{
    max7219_setleds(leds_pio, leds_sm);
}
    

void init_leds()
{
    max7219_pio_setup();
    max7219_init(leds_pio,leds_sm);
}
    

void max7219_pio_setup()
{
    // get an instance of the PIO, select state machine, and add a program to it
    PIO pio = pio0;
    uint sm = 0;
    uint offset = pio_add_program(pio, &max7219_program);

    max7219_program_init(pio, sm, offset, CLOCK_PIN, MOSI_PIN, CS_PIN);
    
    leds_pio = pio;
    leds_sm = sm;
}

void max7219_init(PIO pio, uint sm)
{
    max7219_put(pio, sm, 0x0F00); // test mode
    max7219_put(pio, sm, 0x0B07); // 8 lines
    max7219_put(pio, sm, 0x0A03); // brightness = 15/32
    max7219_put(pio, sm, 0x0900); // disable 7-seg decoding
    max7219_put(pio, sm, 0x0C01); // turn on displays
}

void max7219_setleds(PIO pio, uint sm)
{
    static uint8_t vals[8]={0};
    static uint8_t id[8]={1,2,3,4,5,6,7,8};
    uint16_t data;
    for (int i = 0; i < 8; i++)
    {
        data = (id[i] << 8) + vals[i];
        max7219_put(pio, sm, data++);
        vals[i] = rnd();

    }
}