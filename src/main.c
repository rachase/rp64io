#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "bsp/board.h"
#include "tusb.h"
#include "pico/stdlib.h"
#include "usb_descriptors.h"
#include "infoleds.h"
#include "adc.h"
#include "max7219_leds.h"
#include "switches.h"

void hid_task(void);
void adc_task(void);

#define PICO_LED 25

// container for the data sent to the host
struct __attribute__((packed, aligned(1))) joy_report
{
    uint8_t buttons;         // 1 bytes
    uint16_t axis[NUMAXES];  // 32 bytes
} joy_report[8];

// read all the adcs.  Copy the memory over to the joystick report.
void adc_task()
{
    static uint8_t current_bank = 0;
    static uint8_t current_chan = 0;
    //static uint16_t val = 0;

    uint16_t val = joy8_adc_read(current_bank,current_chan);
    //val++;
    //val = val &0x0FFF;
    // map the bank and chan over to the joystick mem and save val;
    uint8_t joynum,axisnum;
    axisnum = current_chan % 8;
    joynum = current_bank*2 + (uint8_t)(current_chan/8);
    joy_report[joynum].axis[axisnum] = val;
        
    //setup next read
    current_chan++;
    if (current_chan >= NUMADCCHANS)
    {
        current_chan = 0;
        current_bank++;
        if (current_bank>=NUMADCBANKS)
        {
            current_bank = 0;
        }
    }
    
}

int main(void)
{
    // make sure the led is setup, the most important thing of the project
    gpio_init(PICO_LED);
    gpio_set_dir(PICO_LED,true);

    // pico board init
    board_init();

    // init routines. Should setup gpio and pins
    init_leds();
    joy8_setup_adc();
    init_info_leds();
    init_switches();
    // init tiny usb stack
    tusb_init();
   
    // Poll every 1ms
    const uint32_t interval_ms = 10;
    static uint32_t start_ms = 0;
    static uint8_t reportnum = 1;

    bool pico_led_state = false;

    // round robin style tasks
    while (1)
    {
        // update analogs, copy over to joy report 
        adc_task();      

        // get the led info
        leds_rando();

        // read in the switch data, get it, copy to joy report
        switches_task();
        uint64_t switchvalues = switches_get();
        for (int i = 0;i<8;i++)
        {
            uint8_t extractedByte = (uint8_t)((switchvalues >> (i * 8)) & 0xFF);
            joy_report[i].buttons = extractedByte;
        }

        // handle hid and usb tasks
        hid_task();
        tud_task(); 

        // blink board led abnd sleep
        gpio_put(PICO_LED,pico_led_state);
        pico_led_state = !pico_led_state;
        sleep_ms(25);    

    }

    return 0;
}

//--------------------------------------------------------------------+
// tiny USB related functions
//--------------------------------------------------------------------+

void con_panic(uint16_t errcode)
{
    for (int i = 0; i < 8 ; i++)
    {
        joy_report[i].buttons = errcode;
    }
    info_leds_toggle(4);
    while (1)
    {
        tud_task(); // tinyusb device task
        // Remote wakeup
        if (tud_suspended())
        {
            // Wake up host if we are in suspend mode
            // and REMOTE_WAKEUP feature is enabled by host
            tud_remote_wakeup();
        }

        if (tud_hid_ready())
        {
            for (int i = 0; i < 8 ; i++)
            {
                tud_hid_n_report(0x00, i+1, &joy_report[i], sizeof(joy_report[i]));
            }
        }
    }
}

//updates data and sends over to t_usb task.  
void hid_task(void)
{
    // Poll every 1ms
    const uint32_t interval_ms = 50;
    static uint32_t start_ms = 0;

    static uint8_t reportnum = 1;

    if ((board_millis() - start_ms) < interval_ms)
    {
        return; // not enough time
    }

    //enough time, lets do this!
    start_ms += interval_ms;

    // modify the buttons and axis. 
    joy_report[0].buttons*=2;
    if(joy_report[0].buttons==0)joy_report[0].buttons=1;
    // for (int j = 0; j< 2 ; j++)
    // {
    //     joy_report[0].axis[j]+=52;
    //     if(joy_report[0].axis[j]>=0x0FFF)joy_report[0].axis[j]=0;
    // }
    
    // Remote wakeup
    if (tud_suspended())
    {
        // Wake up host if we are in suspend mode
        // and REMOTE_WAKEUP feature is enabled by host
        tud_remote_wakeup();
    }

    // if 
    if (tud_hid_ready())
    {
        tud_hid_n_report(0x00, reportnum, &joy_report[reportnum-1], sizeof(joy_report[reportnum-1]));
        reportnum++;
        if (reportnum >8)
        {
            reportnum=1;
        }
    }
}

//--------------------------------------------------------------------+
// Device callbacks
//--------------------------------------------------------------------+

// Invoked when device is mounted
void tud_mount_cb(void)
{
    info_leds_set(3, 1);
}

// Invoked when device is unmounted
void tud_umount_cb(void)
{
    info_leds_set(3, 0);
}

// Invoked when usb bus is suspended
// remote_wakeup_en : if host allow us  to perform remote wakeup
// Within 7ms, device must draw an average of current less than 2.5 mA from bus
void tud_suspend_cb(bool remote_wakeup_en)
{
    (void)remote_wakeup_en;
}

// Invoked when usb bus is resumed
void tud_resume_cb(void)
{
}

//--------------------------------------------------------------------+
// USB HID
//--------------------------------------------------------------------+

// Invoked when received GET_REPORT control request
// Application must fill buffer report's content and return its length.
// Return zero will cause the stack to STALL request
uint16_t tud_hid_get_report_cb(uint8_t instance, uint8_t report_id, hid_report_type_t report_type, uint8_t *buffer, uint16_t reqlen)
{
    // TODO not Implemented
    (void)report_id;
    (void)report_type;
    (void)buffer;
    (void)reqlen;

    info_leds_toggle(1);
    return 0;
}

// Invoked when received SET_REPORT control request or
// received data on OUT endpoint ( Report ID = 0, Type = 0 )
void tud_hid_set_report_cb(uint8_t instance, uint8_t report_id, hid_report_type_t report_type, uint8_t const *buffer, uint16_t bufsize)
{
    // if (report_id == 2 && report_type == HID_REPORT_TYPE_OUTPUT && buffer[0] == 2 && bufsize >= sizeof(light_data)) //light data
    // {
    //     size_t i = 0;
    //     for (i; i < sizeof(light_data); i++)
    //     {
    //         light_data.raw[i] = buffer[i + 1];
    //     }
    // }

    // // echo back anything we received from host
    // tud_hid_report(0, buffer, bufsize);

      // This example doesn't use multiple report and report ID
    // (void) itf;
    // (void) report_id;
    // (void) report_type;

    // if (report_id == 2 && report_type == HID_REPORT_TYPE_OUTPUT && buffer[0] == 2 && bufsize >= sizeof(light_data)) //light data
    // {
    //     size_t i = 0;
    //     for (i; i < sizeof(light_data); i++)
    //     {
    //         light_data.raw[i] = buffer[i + 1];
    //     }
    // }

    // joy_report.axis[0]=instance;
    // joy_report.axis[1]=report_id;
    // joy_report.axis[2]=(uint8_t)(report_type);
    // joy_report.axis[3]=bufsize;

    //echo back anything we received from host
    //tud_hid_report(0, buffer, bufsize);
    info_leds_toggle(0);
}