/**
 * Copyright (c) 2020 Raspberry Pi (Trading) Ltd.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "pico/stdlib.h"
#include "hardware/pio.h"
#include "hardware/clocks.h"
//#include "max7219.pio.h"
#include "pico.h"

#define CLOCK_PIN  15
#define MOSI_PIN 14
#define CS_PIN 13

#include <stdio.h>
#include "pico/stdlib.h"
#include "hardware/regs/rosc.h"
#include "hardware/regs/addressmap.h"

// uint8_t rnd(void){
//     int k;
//     uint8_t random=0;
//     volatile uint32_t *rnd_reg=(uint32_t *)(ROSC_BASE + ROSC_RANDOMBIT_OFFSET);
    
//     for(k=0;k<8;k++){
    
//     random = random << 1;
//     random=random + (0x01 & (*rnd_reg));

//     }
//     return random;
// }

// void max7219_setup(PIO pio, uint sm)
// {
//     max7219_put(pio, sm, 0x0F00); // test mode
//     max7219_put(pio, sm, 0x0B07); // 8 lines
//     max7219_put(pio, sm, 0x0A03); // brightness = 15/32
//     max7219_put(pio, sm, 0x0900); // disable 7-seg decoding
//     max7219_put(pio, sm, 0x0C01); // turn on displays
// }

// void max7219_setleds(PIO pio, uint sm)
// {
//     static uint8_t vals[8]={0};
//     static uint8_t id[8]={1,2,3,4,5,6,7,8};
//     uint16_t data;
//     for (int i = 0; i < 8; i++)
//     {
//         data = (id[i] << 8) + vals[i];
//         max7219_put(pio, sm, data++);
//         vals[i] = rnd();

//     }

// }

void setBit(unsigned long long *number, int position, int value) {
    // Ensure the position is within the valid range
    if (position < 0 || position >= 64) {
        return;
    }

    // Set the bit at the specified position to the given value
    if (value == 1) {
        *number |= (1ULL << position);
    } else if (value == 0) {
        *number &= ~(1ULL << position);
    } else {
        return;
    }
}


#define GPIO_LATCH 2
#define GPIO_DATA 3
#define GPIO_CLOCK 10
#define COL_SINK_A 9
#define COL_SINK_B 8
#define COL_SINK_C 7

// int main()
// {
//     gpio_init(25);
//     gpio_set_dir(25, GPIO_OUT);
//     gpio_init(0);
//     gpio_set_dir(0, GPIO_OUT);
//     while(1)
//     {
//         gpio_put(25,true);
//         gpio_put(0,true);
//         sleep_ms(1000);
//         gpio_put(0,false);
//         gpio_put(25,false);
//         sleep_ms(1000);
//     }
// }

int main() {

    // const uint LED_PIN = 1;
    // gpio_init(LED_PIN);
    // gpio_set_dir(LED_PIN, GPIO_OUT);

    // // get an instance of the PIO, select state machine, and add a program to it
    // PIO pio = pio0;
    // uint sm = 0;
    // uint offset = pio_add_program(pio, &max7219_program);

    // max7219_program_init(pio, sm, offset, CLOCK_PIN, MOSI_PIN, CS_PIN);

    // max7219_setup(pio,sm);

    // uint16_t data;
    // uint8_t vals[8]={0};

    // while (true) {

    //     gpio_put(LED_PIN, 1);
    //     max7219_setleds(pio, sm);    
    //     sleep_ms(100);
        
    //     gpio_put(LED_PIN, 0);
    //     max7219_setleds(pio, sm);
    //     sleep_ms(100);

    //     if (data > 0x0500) data = 0x0100;
    // }

    gpio_init(25);
    gpio_set_dir(25,GPIO_OUT);

    // while(1)
    // {
    //     gpio_put(0,true);
    //     sleep_ms(200);
    //     gpio_put(0,false);
    //     sleep_ms(200);
    //     pins++;
    // }

    gpio_init(GPIO_LATCH);
    gpio_set_dir(GPIO_LATCH, GPIO_OUT);
    gpio_init(GPIO_DATA);
    gpio_set_dir(GPIO_DATA, GPIO_IN);
    gpio_init(GPIO_CLOCK);
    gpio_set_dir(GPIO_CLOCK, GPIO_OUT);
    gpio_init(COL_SINK_A);
    gpio_set_dir(COL_SINK_A, GPIO_OUT);
    gpio_init(COL_SINK_B);
    gpio_set_dir(COL_SINK_B, GPIO_OUT);
    gpio_init(COL_SINK_C);
    gpio_set_dir(COL_SINK_C, GPIO_OUT);

    uint8_t columnselect = 0;
    uint8_t bit_position = 0;
    bool csa,csb,csc,bit_value = 0;
    uint64_t pins = 0;
    bool output=true;
    while (true)
    {
        gpio_put(25,output);
        output = !output; 
        // select which column to read
        csa = (columnselect & 0x01);
        gpio_put(COL_SINK_A,csa);
        csb = ((columnselect & 0x02) >> 1);
        gpio_put(COL_SINK_B,csb);
        csc = ((columnselect & 0x04) >> 2);
        gpio_put(COL_SINK_C,csc);       

        // bit bang the 74hc165
        
        // set latch low, (should be high)
        gpio_put(GPIO_LATCH,0);
        // delay
        gpio_put(GPIO_LATCH,1);
        // data is in the chip now

        // read in all 8 rows
        for (int i = 0; i < 8 ; i++)
        {
            // set clock high, (should be low)
            gpio_put(GPIO_CLOCK,1);
            // maybe a delay, data is on output now
            // read in the data
            bit_value = gpio_get(GPIO_DATA);
            if(bit_value == false)
            {
                bit_position = columnselect * 8 + i;
                setBit(&pins, bit_position,bit_value);
            }
            // clock low
            gpio_put(GPIO_CLOCK,0);
            // delay
        }
        
        // next column.  
        columnselect++;
        if (columnselect>=8)columnselect = 0;
    }

}