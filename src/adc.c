#include "adc.h"

uint16_t fun_adc_vals[NUMADCBANKS][NUMADCCHANS];

// we are only using one pin so easy enough. 
void joy8_setup_adc()
{
    adc_init();
    // Make sure GPIO is high-impedance, no pullups etc
    adc_gpio_init(ADCPIN);
    adc_select_input(2);

    // set the gpio pins, brute force, garbage style.
    gpio_init(BANKSEL0);
    gpio_set_dir(BANKSEL0, GPIO_OUT);
    gpio_init(BANKSEL1);
    gpio_set_dir(BANKSEL1, GPIO_OUT);
    gpio_init(CHANSEL0);
    gpio_set_dir(CHANSEL0, GPIO_OUT);
    gpio_init(CHANSEL1);
    gpio_set_dir(CHANSEL1, GPIO_OUT);
    gpio_init(CHANSEL2);
    gpio_set_dir(CHANSEL2, GPIO_OUT);
    gpio_init(CHANSEL3);
    gpio_set_dir(CHANSEL3, GPIO_OUT);

    for (int i = 0; i < NUMADCBANKS ; i++)
    {
        for (int j = 0; j < NUMADCCHANS ; j++)
        {
            fun_adc_vals[i][j] = 0;
        }
    }
}

void joy8_set_control_pins(uint8_t bank, uint8_t channel)
{
    // sanity check
    if ((bank >= NUMADCBANKS) || (channel >= NUMADCCHANS))
    {
        gpio_put(BANKSEL0,0);
        gpio_put(BANKSEL1,0);
        gpio_put(CHANSEL0,0);
        gpio_put(CHANSEL1,0);
        gpio_put(CHANSEL2,0);
        gpio_put(CHANSEL3,0);
    }
    else
    {
        gpio_put(BANKSEL0, (bank & 0x01));
        gpio_put(BANKSEL1, ((bank & 0x02)>>1));
        gpio_put(CHANSEL0, (channel & 0x01));
        gpio_put(CHANSEL1, ((channel & 0x02)>>1));
        gpio_put(CHANSEL2, ((channel & 0x04)>>2));
        gpio_put(CHANSEL3, ((channel & 0x08)>>3));
    }

    // gpio_put(BANKSEL0, 1);
    // gpio_put(BANKSEL1, 1);
    // gpio_put(CHANSEL0, (channel & 0x01));
    // gpio_put(CHANSEL1, ((channel & 0x02)>>1));
    // gpio_put(CHANSEL2, ((channel & 0x04)>>2));
    // gpio_put(CHANSEL3, ((channel & 0x08)>>3));        
}

// cycle through adc each time we read.  
uint16_t joy8_adc_read(uint8_t current_bank, uint8_t current_chan)
{
    if (current_bank >= NUMADCBANKS)
    {
        current_bank = 0;
    }

    if (current_chan >= NUMADCCHANS)
    {
        current_chan = 0;
    }
    
    // switch control pins
    joy8_set_control_pins(current_bank, current_chan);
    
    //read the value
    uint16_t val = adc_read();

    return val;

}