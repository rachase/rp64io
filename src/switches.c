#include "switches.h"


#define GPIO_LATCH 2
#define GPIO_DATA 3
#define GPIO_CLOCK 10
#define COL_SINK_A 9
#define COL_SINK_B 8
#define COL_SINK_C 7


uint64_t switches = 0;
void setBit(unsigned long long *number, int position, int value);

void init_switches()
{
    gpio_init(GPIO_LATCH);
    gpio_set_dir(GPIO_LATCH, GPIO_OUT);
    gpio_init(GPIO_DATA);
    gpio_set_dir(GPIO_DATA, GPIO_IN);
    gpio_init(GPIO_CLOCK);
    gpio_set_dir(GPIO_CLOCK, GPIO_OUT);
    gpio_init(COL_SINK_A);
    gpio_set_dir(COL_SINK_A, GPIO_OUT);
    gpio_init(COL_SINK_B);
    gpio_set_dir(COL_SINK_B, GPIO_OUT);
    gpio_init(COL_SINK_C);
    gpio_set_dir(COL_SINK_C, GPIO_OUT);
}

void setBit(unsigned long long *number, int position, int value) {
    // Ensure the position is within the valid range
    if (position < 0 || position >= 64) {
        return;
    }

    // Set the bit at the specified position to the given value
    if (value == 1) {
        *number |= (1ULL << position);
    } else if (value == 0) {
        *number &= ~(1ULL << position);
    } else {
        return;
    }
}

void switches_task()
{
    uint8_t bit_position = 0;
    bool csa,csb,csc,bit_value = 0;
    switches = 0;
    bool output=true;

    // for every column
    for (int col = 0; col < 8 ; col++)
    {
        gpio_put(25,output);
        output = !output; 
        // select which column to read
        csa = (col & 0x01);
        gpio_put(COL_SINK_A,csa);
        csb = ((col & 0x02) >> 1);
        gpio_put(COL_SINK_B,csb);
        csc = ((col & 0x04) >> 2);
        gpio_put(COL_SINK_C,csc);       

        // bit bang the 74hc165
        
        // set latch low, (should be high)
        gpio_put(GPIO_LATCH,0);
        // delay
        gpio_put(GPIO_LATCH,1);
        // data is in the chip now

        // read in all 8 rows
        for (int row = 0; row < 8 ; row++)
        {
            // set clock high, (should be low)
            gpio_put(GPIO_CLOCK,1);
            // maybe a delay, data is on output now
            // read in the data
            bit_value = gpio_get(GPIO_DATA);
            bit_position = col * 8 + row;
            setBit(&switches, bit_position,bit_value);
            //if(bit_value == false)
            //{
            //    bit_position = columnselect * 8 + i;
            //    setBit(&pins, bit_position,bit_value);
            //}
            // clock low
            gpio_put(GPIO_CLOCK,0);
            // delay
        }
    }
}

uint64_t switches_get()
{
    return switches;
}
